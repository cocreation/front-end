import React from 'react';
import {
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  IconButton,
} from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import ComponentCard from '../../Components/ComponentCard';
import { getComponents, createCompoent, contributeToComponent, likeCategoryA, likeComponent } from '../../Utils/api';
import { withNamespaces } from 'react-i18next';

class CreateComponent extends React.Component {
  state = {
    components: [],
    newCompoentTitle: '',
    open: false,
    contributeOpen: false,
    contributeTitle: '',
    componentToContribute: null,
  };

  componentDidMount() {
    const { mainCategory, secondaryCategory } = this.props;

    getComponents(
      mainCategory._id,
      secondaryCategory !== 'none' ? secondaryCategory._id : 'none',
    ).then(resposne => {
      if (resposne.status === 200) {
        this.setState({
          components: resposne.data.data,
        });
      }
    }).catch(error => {
      if (error.response) {
        const serverError = error.response.data.data;
        this.props.errorHandler(new Error(serverError.message));
      }
    });
  }

  handleContribute = () => {
    const { componentToContribute, contributeTitle } = this.state;
    const { contentLanguage } = this.props;
    contributeToComponent(componentToContribute, contributeTitle, contentLanguage)
      .then(response => {
        if(response.status === 201){
          this.setState({
            contributeOpen: false,
            contributeTitle: '',
            componentToContribute: null,
          }, () => {
            const { mainCategory, secondaryCategory } = this.props;

            getComponents(
              mainCategory._id,
              secondaryCategory !== 'none' ? secondaryCategory._id : 'none',
            ).then(resposne => {
              if (resposne.status === 200) {
                this.setState({
                  components: resposne.data.data,
                });
              }
            }).catch(error => {
              if (error.response) {
                const serverError = error.response.data.data;
                this.props.errorHandler(new Error(serverError.message));
              }
            });
          })
        }
      }).catch(error => {
      if (error.response) {
        const serverError = error.response.data;
        this.props.errorHandler(new Error(serverError.message));
      }
    });
  };

  handeleOnLikeClick = id => {
    likeComponent(id)
      .then(response => {
        if (response.status === 200) {
          this.setState(prevState => {
            return {
              ...prevState,
              components: prevState.components.map(c => {
                if (c._id === id) {
                  return {
                    ...c,
                    likes: c.likes + 1,
                  };
                } else {
                  return {
                    ...c,
                  };
                }
              }),
            };
          });
        }
      }).catch(error => {
      console.log(error);
    });
  }

  handelCreateCompoent = () => {
    const title = this.state.newCompoentTitle;
    const { mainCategory, secondaryCategory, contentLanguage } = this.props;

    createCompoent({
      title,
      categoryA: mainCategory._id,
      categoryB: secondaryCategory !== 'none' ? secondaryCategory._id : 'none',
      contentLanguage,
    }).then(response => {
      if (response.status === 201) {
        this.setState(prevState => ({
          components: [...prevState.components, response.data.data],
          open: false,
        }));
      }
    }).catch(error => {
      if (error.response) {
        const serverError = error.response.data.data;
        this.props.errorHandler(new Error(serverError.message));
      }
    });
  };

  render() {
    const { components, newCompoentTitle, open, contributeOpen, contributeTitle } = this.state;
    const { mainCategory, secondaryCategory, contentLanguage, t } = this.props;

    return (
      <Grid container direction="column">
        <Grid item xs={12}>
          <IconButton onClick={this.props.onBack}>
            <ArrowBack/>
          </IconButton>
          <Button fullWidth onClick={() => this.setState({ open: true })}>
            Create Component
          </Button>
        </Grid>

        {components.map(component => (
          <ComponentCard
            title={component.title}
            id={component._id}
            contentLanguage={contentLanguage}
            languages={component.languages}
            likes={component.likes}
            onLikeClick={this.handeleOnLikeClick}
            onFollowClick={() => {
            }}
            onValueClick={() => {
            }}
            onCoCreationClick={() => {
            }}
            onContributeClick={(id) => this.setState({ contributeOpen: true, componentToContribute: id })}
            onClickCallBack={this.props.onCallbackClick}
          />
        ))}
        <Dialog open={open}>
          <DialogTitle>Create Component</DialogTitle>
          <DialogContent>
            <TextField
              value={newCompoentTitle}
              onChange={event => this.setState({ newCompoentTitle: event.target.value })}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.setState({ open: false })}>Cancel</Button>
            <Button onClick={this.handelCreateCompoent}>Create</Button>
          </DialogActions>
        </Dialog>
        <Dialog open={contributeOpen}
                onBackdropClick={() => this.setState({
                  contributeOpen: false,
                  contributeTitle: '',
                  componentToContribute: null,
                })}>
          <DialogTitle>{t('contribute_title')}</DialogTitle>
          <DialogContent>
            <TextField fullWidth style={{ margin: 4 }} label={t('component_title')} variant={'outlined'}
                       value={contributeTitle} onChange={event => this.setState({
              contributeTitle: event.target.value,
            })}/>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleContribute}>{t('submit')}</Button>
            <Button onClick={() => this.setState({
              contributeOpen: false,
              contributeTitle: '',
              componentToContribute: null,
            })}>{t('cancel')}</Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
  }
}

export default withNamespaces('translation')(CreateComponent);
