import React from 'react';
import { Grid } from '@material-ui/core';
import SelectCard from '../../Components/LanguageCard';
import { getCategoryAList, likeCategoryA, likeCategoryB } from '../../Utils/api';

class SelectMainCategory extends React.Component {
  state = {
    categories: [],
  };

  componentDidMount() {
    getCategoryAList()
      .then(response => {
        if (response.status === 200) {
          this.setState({
            categories: response.data.data,
          });
        }
      });
  }

  handleOnItemClick = id => {
    this.props.handleOnMainCategoryClick(this.state.categories.filter(i => i._id === id)[0]);
  };

  handleOnLikeClick = id => {
    likeCategoryA(id)
      .then(response => {
        if (response.status === 200) {
          this.setState(prevState => {
            return {
              ...prevState,
              categories: prevState.categories.map(c => {
                if (c._id === id) {
                  return {
                    ...c,
                    likes: c.likes + 1,
                  };
                } else {
                  return {
                    ...c,
                  };
                }
              }),
            };
          });
        }
      }).catch(error => {
      console.log(error);
    });
  };

  handleOnBackClick = () => {
    this.props.onBack();
  };

  render() {
    const { categories } = this.state;
    return (
      <Grid container>
        {categories.map(cat => (
          <SelectCard
            id={cat._id}
            title={cat.title}
            likes={cat.likes}
            onClickCallBack={this.handleOnItemClick}
            onLikeClick={this.handleOnLikeClick}
          />
        ))}
      </Grid>
    );
  }
}

export default SelectMainCategory;
