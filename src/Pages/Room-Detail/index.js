import React from 'react';
import { Grid, TextField, Button, Typography, IconButton } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';
import Divider from '@material-ui/core/Divider';
import { ArrowBack } from '@material-ui/icons';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { createPost, getPosts, getRoomDetails } from '../../Utils/api';

class RoomDetails extends React.Component {
  state = {
    openCreatePost: false,
    postTitle: '',
    postBody: '',
    posts: [],
  };


  componentDidMount() {
    getRoomDetails(this.props.match.params.roomId)
      .then(response => {
        if (response.status === 201) {
          this.setState({
            room: response.data.data,
          }, () => {
            getPosts(this.props.match.params.roomId)
              .then(response => {
                if (response.status === 201) {
                  this.setState({
                    posts: response.data.data,
                  });
                }
              });
          });
        }
      });

  }

  handleOnFollowItemClick = id => {
  };

  handleOnBackClick = () => {
    this.props.onBack();
  };

  handleCreatePost = () => {
    const { room, postBody, postTitle } = this.state;
    createPost(room._id, postTitle, postBody)
      .then(response => {
        if (response.status === 201) {
          getPosts(room._id)
            .then(response => {
              if (response.status === 201) {
                this.setState({
                  posts: response.data.data,
                  openCreatePost: false,
                  postTitle: '',
                  postBody: '',
                });
              }
            });
        }
      });
  };

  render() {
    const { t, contentLanguage } = this.props;
    const { room, openCreatePost, postBody, postTitle, posts } = this.state;
    if (room === undefined) {
      return <div/>;
    }
    return (
      <Grid container>
        <Grid item sm={12} md={12}>
          {Object.keys(room.component.title).map(key => (
            <Typography>{key} : {room.component.title[key]}</Typography>
          )) }
        <Typography>{t(room.title)}/{t(room.variable)}</Typography>
        <Divider/>
        {room.questions.map(q =>
          <Typography key={q._id} style={{ margin: 4 }}>{t(q.title)}</Typography>
        )}

        <Button variant={'contained'} fullWidth style={{ margin: 4 }}
                onClick={() => this.setState({ openCreatePost: true })}>{t('create_post')}</Button>

        <hr/>
        {posts.map(post => (
          <Grid key={post._id} item xs={12}>
            <Typography variant={'h3'}>{post.title}</Typography>
            <Typography variant={'body1'}>{post.body}</Typography>
            <hr/>
          </Grid>
        ))}
        </Grid>
        <Dialog open={openCreatePost} onBackdropClick={() => this.setState({
          openCreatePost: false,
          postTitle: '',
          postBody: '',
        })}>
          <DialogTitle>{t('create_post_title')}</DialogTitle>
          <DialogContent>
            <TextField fullWidth style={{ margin: 4 }} label={t('post_title')} value={postTitle} variant={'outlined'}
                       onChange={event => this.setState({ postTitle: event.target.value })}/>
            <TextField fullWidth style={{ margin: 4 }} label={t('post_body')} value={postBody} variant={'outlined'}
                       onChange={event => this.setState({ postBody: event.target.value })} multiline/>
          </DialogContent>

          <DialogActions>
            <Button onClick={this.handleCreatePost}>{t('submit')}</Button>
            <Button onClick={() => this.setState({
              openCreatePost: false,
              postTitle: '',
              postBody: '',
            })}>{t('cancel')}</Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
  }
}

export default withNamespaces('translation')(RoomDetails);
