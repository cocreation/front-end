import React from 'react';
import {
    Grid,
    IconButton,
} from '@material-ui/core';
import {ArrowBack, Filter, FilterList} from '@material-ui/icons';
import {withStyles} from '@material-ui/core/styles';
import {getRooms, getPosts} from '../../Utils/api';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import {withNamespaces} from 'react-i18next';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import PostCard from '../../Components/Post-Card';
import Chat from "../../Components/Chat";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Tooltip from "@material-ui/core/Tooltip";

const styles = theme => ({
    relative: {
        position: 'relative',
    },
    fillBody: {
        height: '70vh',
    },
    scrollable: {
        overflowY: 'scroll',
    },
    filterItem: {
        width: '100%',
        margin: theme.spacing(0.2),
    },
    fullWidthCol: {
        width: '100%',
    },
    filterActive: {
        backgroundColor: 'grey',
    },
    fab: {
        position: 'fixed',
        bottom: 30,
        right: 30,
    },
});

class CreateRoom extends React.Component {
    state = {
        rooms: [],
        newRoomTitle: '',
        open: false,
        roomFilter: undefined,
        posts: [],
        anchorEl: null,
    };

    componentDidMount() {
        const {componentId, variableId} = this.props.match.params;
        getRooms(
            componentId,
            variableId,
        ).then(resposne => {
            if (resposne.status === 200) {
                this.setState({
                    rooms: resposne.data.data,
                });
            }
        });
    }


    handleOnBackClick = () => {
        this.props.history.goBack();
    };
    handleOnRoomFilterClick = (room) => () => {
        this.setState({
            roomFilter: room,
        }, this.fetchPosts);
    };

    fetchPosts = () => {
        const {roomFilter} = this.state;
        if (roomFilter !== undefined) {
            getPosts(roomFilter._id)
                .then(response => {
                    this.setState({posts: response.data.data});
                }).catch(err => {
                console.log(err);
            });
        }
    };

    handleOnFilterByCategory = category => () => {
        if (category) {
            this.setState({
                filterCategory: category
            })
        }
        this.setState({
            anchorEl: null
        })
    }

    render() {
        const {rooms, roomFilter, posts, anchorEl, filterCategory} = this.state;
        const {componentId, variableId} = this.props.match.params;
        const {classes, t} = this.props;
        let showingPosts = roomFilter ? posts : rooms.map(room => {
            const posts = room.posts;
            return posts.map(post => ({
                ...post,
                room,
            }));
        }).flat(1);
        if (filterCategory && filterCategory !== 'All') {
            showingPosts = showingPosts.filter(p => p.categories && p.categories.includes(filterCategory))
        }
        return (
            <Grid container direction="row">

                <Grid item sm={false} md={3} className={`${classes.fillBody} ${classes.scrollable}`}>
                    <Chat roomId={roomFilter ? roomFilter._id : undefined}/>
                </Grid>
                <Grid item sm={12} lg={6}>
                    <Grid container
                          direction="row"
                          justify="space-between"
                          alignItems="center">
                        <IconButton onClick={this.handleOnBackClick}>
                            <ArrowBack/>
                        </IconButton>
                        <Button variant={'contained'}
                                style={{flex: 1}}
                                color={'primary'}
                                onClick={() => this.props.history.push(`/main/component/${componentId}/${variableId}/createPost`)}>Create
                            Post</Button>
                        <Tooltip title={'Filter by category'}>
                            <IconButton onClick={event => this.setState({anchorEl: event.currentTarget})}>
                                <FilterList/>
                            </IconButton>
                        </Tooltip>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={this.handleOnFilterByCategory(null)}
                        >
                            {[
                                'All',
                                'Inform',
                                'Community',
                                'Instances',
                                'Initiatives',
                                'Events',
                                'Offer',
                                'Survey',
                                'Jobs',
                                'Source funding',
                                'Crowdfunding',
                                'Coworking'
                            ].map((category) => (
                                <MenuItem key={category}
                                          onClick={this.handleOnFilterByCategory(category)}>{category}</MenuItem>
                            ))}


                        </Menu>
                    </Grid>

                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="stretch"
                        className={`${classes.fillBody} ${classes.scrollable}`}
                    >
                        {showingPosts.map(post => (
                            <PostCard
                                key={post._id}
                                title={post.title}
                                content={post.body}
                                image={post.coverImage ? post.coverImage.filename : ''}
                                room={roomFilter ? roomFilter.title : post.room.title}
                            />
                        ))}
                    </Grid>
                </Grid>

                <Grid item sm={false} md={3}
                      className={`${classes.fillBody} ${classes.scrollable} ${classes.fullWidthCol}`}>
                    <List component="nav" aria-label="main mailbox folders"
                    >
                        <ListItem button className={`${classes.filterItem} ${roomFilter ? '' : classes.filterActive}`}
                                  onClick={this.handleOnRoomFilterClick(undefined)}>
                            <ListItemText>{t('all')}</ListItemText>
                        </ListItem>
                        <Divider/>
                        {rooms.map(room => (
                            <React.Fragment key={room._id}>
                                <ListItem button
                                          className={`${classes.filterItem} ${roomFilter ? (roomFilter._id === room._id ? classes.filterActive : '') : ''}`}
                                          onClick={this.handleOnRoomFilterClick(room)}>
                                    <ListItemText>{t(room.title)}</ListItemText>
                                </ListItem>
                                <Divider/>
                            </React.Fragment>
                        ))}
                    </List>
                </Grid>
            </Grid>
        );
    }
}

export default withNamespaces('translation')(withStyles(styles)(CreateRoom));
