import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withNamespaces } from 'react-i18next';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { getRooms, getVariables, likeRoom } from '../../Utils/api';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import RoomCard from '../../Components/RoomCard';

const styles = theme => ({
  relative: {
    position: 'relative',
  },
  fillBody: {
    height: '80vh',
  },
  scrollable: {
    overflowY: 'scroll',
  },
});

class ComponentDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      variables: [],
      selectedTab: null,
    };
  }

  componentDidMount() {
    getVariables().then(response => {
      if (response.status === 200) {
        this.setState({
          variables: response.data.data,
          selectedTab: response.data.data[0].title,
        }, this.fetchNewRooms);
      }
    }).catch(error => {

    });
  }

  handleOnTabClick = (event, newValue) => {
    this.setState({
      selectedTab: newValue,
    }, this.fetchNewRooms);
  };

  fetchNewRooms = () => {
    const { componentId } = this.props.match.params;
    getRooms(componentId, this.state.selectedTab)
      .then(response => {
        if (response.status === 200) {
          this.setState({
            rooms: response.data.data
          })
        }
      });
  };

  handleOnRoomClick = (room) => {
    this.props.history.push(`/main/room/${room._id}`)
  };

  handleLikeClick = id => {
    likeRoom(id)
      .then(response => {
        if (response.status === 200) {
          this.setState(prevState => {
            return {
              ...prevState,
              rooms: prevState.rooms.map(c => {
                if (c._id === id) {
                  return {
                    ...c,
                    likes: c.likes + 1,
                  };
                } else {
                  return {
                    ...c,
                  };
                }
              }),
            };
          });
        }
      }).catch(error => {
      console.log(error);
    });
  }
  render() {
    const { classes, t } = this.props;
    const { variables, selectedTab, rooms } = this.state;
    return (
      <Grid container alignContent={'flex-start'}
            alignItems={'flex-start'} justify={'center'} className={`${classes.fillBody} ${classes.scrollable}`}>
        <Grid item sm={12}>
          <Grid item xs={12}>
            <Tabs centered indicatorColor="primary"
                  value={selectedTab}
                  onChange={this.handleOnTabClick}
                  variant="scrollable"
                  scrollButtons="auto">
              {variables.map(variable => (
                <Tab label={t(variable.title)} value={variable.title}/>
              ))}
            </Tabs>
          </Grid>

          <Grid item xs={12}>
            {rooms ? rooms.map(room => (
              <RoomCard
                room={room}
                title={room.title}
                id={room._id}
                likes={room.likes}
                onLikeClick={this.handleLikeClick}
                onFollowClick={() => {
                }}
                onValueClick={() => {
                }}
                onCoCreationClick={() => {
                }}
                onClickCallBack={this.handleOnRoomClick}
              />
            )) : null}
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`wrapped-tabpanel-${index}`}
      aria-labelledby={`wrapped-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

export default withNamespaces('translation')(withStyles(styles)(ComponentDetails));
