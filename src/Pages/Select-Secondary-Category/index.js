import React from 'react';
import { Grid, IconButton } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import SelectCard from '../../Components/LanguageCard';
import { getCategoryBList, likeCategoryB } from '../../Utils/api';
import main from 'jss-rtl';

class SelectSecondaryCategory extends React.Component {
  state = {
    categories: [],
  };

  componentDidMount() {
    const { mainCategory } = this.props;
    console.log(mainCategory);
    getCategoryBList(mainCategory._id)
      .then(response => {
        if (response.status === 200) {
          this.setState({
            categories: response.data.data,
          });
        }
      }).catch(error => {

    });
  }

  handleOnItemClick = id => {
    this.props.handleOnSecondaryCategoryClick(this.state.categories.filter(i => i._id === id)[0]);
  };

  handleOnLikeClick = id => {
    likeCategoryB(id)
      .then(response => {
        if (response.status === 200) {
          this.setState(prevState => {
            return {
              ...prevState,
              categories: prevState.categories.map(c => {
                if(c._id === id){
                  return {
                    ...c,
                    likes: c.likes + 1
                  }
                }else {
                  return {
                    ...c
                  }
                }
              })
            }
          })
        }
      }).catch(error => {
      console.log(error);
    });
  };

  handleOnBackClick = () => {
    this.props.onBack();
  };

  render() {
    const { categories } = this.state;
    return (
      <Grid container>
        <Grid item xs={12}>
          <IconButton onClick={this.handleOnBackClick}>
            <ArrowBack/>
          </IconButton>
        </Grid>
        {categories.map(cat => (
          <SelectCard
            id={cat._id}
            title={cat.title}
            likes={cat.likes}
            onClickCallBack={this.handleOnItemClick}
            onLikeClick={this.handleOnLikeClick}
          />
        ))}
      </Grid>
    );
  }
}

export default SelectSecondaryCategory;
