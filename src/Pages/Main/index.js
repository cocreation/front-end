import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Grid, IconButton } from '@material-ui/core';
import SelecLanguage from '../Select-Content-Language';
import SelectMainCategory from '../Select-Main-Category';
import SelectSecondaryCategory from '../Select-Secondary-Category';
import CreateComponent from '../Create-Component';
import CreateRoom from '../Create-Room';
import CollectiveReality from '../../Components/Collective-Reality';
import SelectVariables from '../Select-variables';
import Options from '../../Components/Options';
import MyReality from '../../Components/My-Reality';
import { Language } from '@material-ui/icons';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import { withNamespaces } from 'react-i18next';
import RoomDetails from '../Room-Detail';
import Snackbar from '@material-ui/core/Snackbar';
import i18n from '../../i18n';

const styles = theme => ({
  relative: {
    position: 'relative',
  },
  fillBody: {
    height: '88vh',
  },
  scrollable: {
    overflowY: 'scroll',
  },
});

class MainPage extends React.Component {
  state = {
    mainCategory: 'none',
    secondary: 'none',
    component: 'none',
    variable: 'none',
    contentLanguage: 'en',
    selectContentLanguageOpen: false,
    room: 'none',
    hasError: false,
    error: null,
  };

  onLanguageSelect = language => {
    this.setState({
      contentLanguage: language.title,
    });
  };

  onMainCategorySelect = category => {
    this.setState({
      mainCategory: category,
    });
  };

  onSecondaryCategorySelect = category => {
    this.setState({
      secondary: category,
    });
  };

  onComponentSelect = component => {
    this.setState({
      component: component,
    });
  };

  onVariableSelect = variable => {
    this.setState({
      variable: variable,
    });
  };

  handleOnBackToLanguage = () => {
    this.setState({
      contentLanguage: 'none',
    });
  };

  handleOnBackToMain = () => {
    this.setState({
      mainCategory: 'none',
      secondary: 'none',
      variable: 'none',
      component: 'none',
    });
  };

  handleBackToSecondary = () => {
    this.setState({
      secondary: 'none',
      mainCategory: 'none',
    });
  };

  handleBackToComponent = () => {
    this.setState({
      component: 'none',
    });
  };

  handleBacktoVariable = () => {
    this.setState({
      variable: 'none',
    });
  };

  handleBacktoSelectRoom = () => {
    this.setState({
      room: 'none',
    });
  };
  handleOpenSelectLanguage = () => {
    this.setState({
      selectContentLanguageOpen: true,
    });
  };

  handleCloseSelectLanguage = () => {
    this.setState({
      selectContentLanguageOpen: false,
    });
  };

  handleSelectContentLanguage = (lan) => () => {
  };

  handleOnRoomClick = room => {
    this.setState({
      room,
    });
  };

  handleShowError = error => {
    this.setState({
      hasError: true,
      error,
    });
  };

  render() {
    const { classes, history, t } = this.props;
    const { contentLanguage, mainCategory, secondary, component, variable, selectContentLanguageOpen, room, hasError, error } = this.state;

    if (history.location.pathname === '/main') {
      if (mainCategory === 'none') {
        return <Redirect to="/main/category"/>;
      }
    }
    if (history.location.pathname === '/main/secondary') {
      if (mainCategory === 'none') {
        return <Redirect to="/main/category"/>;
      }
    }

    if (history.location.pathname === '/main/secondary') {
      if (mainCategory !== 'none' && secondary !== 'none' && contentLanguage === 'none') {
        return <Redirect to="/main/components"/>;
      }
      // if (mainCategory !== 'none' && mainCategory.id === 4 && contentLanguage === 'none') {
      //   return <Redirect to="/main/content-language" />;
      // }
    }

    if (history.location.pathname === '/main/category') {
      if (mainCategory !== 'none') {
        if (mainCategory.title !== 'cat_a_whole') {
          return <Redirect to="/main/secondary"/>;
        } else {
          return <Redirect to="/main/components"/>;
        }
      }
    }
    if (history.location.pathname === '/main/content-language') {
      if (mainCategory !== 'none') {
        if (mainCategory.id !== 4 && secondary === 'none') {
          return <Redirect to="/main/secondary"/>;
        }
        if (contentLanguage !== 'none') {
          return <Redirect to="/main/components"/>;
        }
      } else {
        return <Redirect to="/main/category"/>;
      }
    }

    if (history.location.pathname === '/main/components') {
      if (mainCategory !== 'none') {
        if (mainCategory.title !== 'cat_a_whole') {
          if (secondary === 'none') {
            return <Redirect to="/main/secondary"/>;
          }
        } else if (contentLanguage === 'none') {
          return <Redirect to={'/main/components'}/>;
        }
      } else if (mainCategory === 'none') {
        return <Redirect to="/main/category"/>;
      }
      if (component !== 'none') {
        return <Redirect to="/main/variables"/>;
      }
    }

    if (history.location.pathname === '/main/variables') {
      if (component === 'none') {
        return <Redirect to="/main/components"/>;
      }
      if (variable !== 'none') {
        return <Redirect to="/main/room"/>;
      }
    }

    if (history.location.pathname === '/main/room') {
      if (variable === 'none') {
        return <Redirect to="/main/variables"/>;
      }
    }

    if (history.location.pathname === '/main/secondary') {
      if (secondary !== 'none') {
        return <Redirect to="/main/components"/>;
      }
    }

    if (history.location.pathname === '/main/room') {
      if (room !== 'none') {
        return <Redirect to={'/main/room-show'}/>;
      }
    }

    if (history.location.pathname === '/main/room-show') {
      if (room === 'none') {
        return <Redirect to={'/main/room'}/>;
      }
    }


    return (
      <Grid container alignContent={'center'}
            alignItems={'center'} justify={'center'}>
        <Grid
          id="menu-side-bar"
          item
          md={2}
          sm={0}
          className={`${classes.fillBody} ${classes.relative}`}
          hidden
        >
          <CollectiveReality/>
        </Grid>
        <Grid
          id="content"
          item
          md={5}
          sm={12}
          className={`${classes.fillBody} ${classes.scrollable}`}
        >
          <Grid item xs={12}>
            <IconButton onClick={this.handleOpenSelectLanguage}>
              <Language/>
            </IconButton>
          </Grid>
          <Switch>
            <Route
              path={'/main/category'}
              exact
              render={({ history }) => (
                <SelectMainCategory
                  history={history}
                  handleOnMainCategoryClick={this.onMainCategorySelect}
                  errorHandler={this.handleShowError}
                  onBack={this.handleOnBackToLanguage}
                />
              )}
            />
            <Route
              path={'/main/secondary'}
              exact
              render={({ history }) => (
                <SelectSecondaryCategory
                  history={history}
                  mainCategory={this.state.mainCategory}
                  errorHandler={this.handleShowError}
                  handleOnSecondaryCategoryClick={this.onSecondaryCategorySelect}
                  onBack={this.handleOnBackToMain}
                />
              )}
            />
            <Route
              path={'/main/components'}
              exact
              render={({ history }) => (
                <CreateComponent
                  history={history}
                  mainCategory={this.state.mainCategory}
                  secondaryCategory={this.state.secondary}
                  contentLanguage={contentLanguage}
                  errorHandler={this.handleShowError}
                  onCallbackClick={this.onComponentSelect}
                  onBack={this.handleBackToSecondary}
                />
              )}
            />
            <Route
              path={'/main/variables'}
              exact
              render={({ history }) => (
                <SelectVariables
                  history={history}
                  mainCategory={this.state.mainCategory}
                  secondaryCategory={this.state.secondary}
                  onBack={this.handleBackToComponent}
                  errorHandler={this.handleShowError}
                  onClickCallBack={this.onVariableSelect}
                />
              )}
            />

            <Route
              path={'/main/room'}
              exact
              render={({ history }) => (
                <CreateRoom
                  history={history}
                  component={this.state.component}
                  variable={this.state.variable}
                  onBack={this.handleBacktoVariable}
                  errorHandler={this.handleShowError}
                  handleOnRoomClick={this.handleOnRoomClick}
                />
              )}
            />
            <Route
              path={'/main/room-show'}
              exact
              render={({ history }) => (
                <RoomDetails
                  history={history}
                  room={room}
                  contentLanguage={contentLanguage}
                  errorHandler={this.handleShowError}
                  onBack={this.handleBacktoSelectRoom}
                />
              )}
            />
          </Switch>
        </Grid>
        <Grid id="dynamic-bar" item md={2} className={`${classes.fillBody}`} hidden>
          <Options/>
        </Grid>

        <Grid id="My-bar" item md={3} className={`${classes.fillBody}`} hidden>
          <MyReality/>
        </Grid>
        <Dialog open={selectContentLanguageOpen} onBackdropClick={this.handleCloseSelectLanguage}>
          <DialogTitle>
            <Typography>{t('choose_language')}</Typography>
          </DialogTitle>
          <DialogContent>
            {[{ key: 'lan_en', language: 'en' }, { key: 'lan_de', language: 'de' }, { key: 'lan_fa', language: 'fa' }]
              .map(lan => (
                <Button variant={'text'} fullWidth
                        onClick={this.handleSelectContentLanguage(lan.language)}>{t(lan.key)}</Button>
              ))}

          </DialogContent>
        </Dialog>
        <Snackbar open={hasError} message={hasError ? error.message : null} autoHideDuration={5000}

                  onClose={() => this.setState({ hasError: false })}/>
      </Grid>
    );
  }
}

export default withNamespaces('translation')(withStyles(styles)(MainPage));
