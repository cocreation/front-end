import React from 'react';
import { Grid, Typography, IconButton } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import LanguageCard from '../../Components/LanguageCard';
import { withNamespaces } from 'react-i18next';

class ChooseLanguage extends React.Component {
  state = {
    languages: [
      { id: 1, title: 'lan_de' },
      { id: 2, title: 'lan_en' },
      { id: 3, title: 'lan_fa' },
      { id: 4, title: 'lan_sp' },
    ],
  };

  handleOnItemClick = id => {
    this.props.handleOnLanguageClick(this.state.languages.filter(i => i.title === id)[0]);
  };

  handleOnFollowItemClick = id => {};

  render() {
    const { languages } = this.state;
    const { t } = this.props;
    return (
      <Grid container direction={'column'} justify={'center'} alignItems={'center'}>
        <Grid item xs={12}>
          <IconButton onClick={this.props.onBack}>
            <ArrowBack />
          </IconButton>
        </Grid>
        <Typography variant={'subtitle1'}>{t('choose_language')}</Typography>
        {languages.map(language => (
          <LanguageCard
            id={language.title}
            title={language.title}
            onClickCallBack={this.handleOnItemClick}
            onFollowClick={this.handleOnFollowItemClick}
          />
        ))}
      </Grid>
    );
  }
}

export default withNamespaces('translation')(ChooseLanguage);
