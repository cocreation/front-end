import * as d3 from 'd3';

const draw = (variables, arrows, componentName, onItemClick) => {
  d3.select('#variables > *').remove();


  // compute window size
  const width = Math.max(document.documentElement.clientWidth, window.innerWidth) || 0;
  const height = document.getElementById('variables').style.height.substr(0, 3);//Math.max(document.documentElement.clientHeight, window.innerHeight) || 0;
  const cell_width = width / 6;
  const cell_height = height / 3;
  d3.select('#variables').append('svg')
    .attr('height', height)
    .attr('width', width)
    .attr('id', `variable-svg`);

  let intervals = [];
  variables.forEach((item, index) => {
    const variable_x = (item.position.col * cell_width) - (cell_width / 2);
    const variable_y = (item.position.row * cell_height) - (cell_height / 2);
    d3.select('#variable-svg')
      .append('ellipse')
      .on('click', onItemClick(item.id))
      .attr('stroke', 'blue')
      .attr('stroke-width', '2px')
      .attr('fill', 'white')
      .attr('cx', variable_x)
      .attr('cy', variable_y)
      .attr('rx', cell_width / 3)
      .attr('ry', cell_height / 3)
      .attr('id', `variable-svg-${item.id}`);

    const titles = item.title.split('/');
    d3.select(`#variable-svg`)
      .append('text')
      .on('click', onItemClick(item.id))
      .attr('x', variable_x)
      .attr('y', variable_y)
      .attr('stroke', 'black')
      .attr('id', `variable-svg-${item.id}-name`)
      .style('font-size', 14)
      .style('text-anchor', 'middle')
      .text(titles[Math.floor((Math.random() * 3))]);
    const intervalId = setInterval(() => {
      d3.select(`#variable-svg-${item.id}-name`).remove();
      d3.select(`#variable-svg`)
        .append('text')
        .on('click', onItemClick(item.id))
        .attr('x', variable_x)
        .attr('y', variable_y)
        .attr('stroke', 'black')
        .attr('id', `variable-svg-${item.id}-name`)
        .style('font-size', 14)
        .style('text-anchor', 'middle')
        .text(titles[Math.floor((Math.random() * 3))]);
    }, 5000);
    intervals = [...intervals, intervalId];

  });

  arrows.forEach((item, index) => {
    const fromRow = item.from.row;
    const fromCol = item.from.col;
    const toRow = item.to.row;
    const toCol = item.to.col;
    const startX = cell_width * fromCol;
    const startY = (cell_height * fromRow) - (cell_height / 2);
    const endX = cell_width * (toCol - 1);
    const endY = (cell_height * toRow) - (cell_height / 2);
    console.log('hey', startY, startY, endX, endY);
    d3.select('#variable-svg')
      .append('line')
      .style('stroke', 'lightgreen')
      .style('stroke-width', 1)
      .attr('x1', startX - 50)
      .attr('y1', startY)
      .attr('x2', endX + 50)
      .attr('y2', endY);
  });
  const component_x = (3.5 * cell_width) - (cell_width / 2);
  const component_y = (2 * cell_height) - (cell_height / 2);
  d3.select('#variable-svg')
    .append('ellipse')
    .attr('stroke', 'green')
    .attr('stroke-width', '2px')
    .attr('fill', 'none')
    .attr('cx', component_x)
    .attr('cy', component_y)
    .attr('rx', cell_width / 1.5)
    .attr('ry', cell_height / 3)
    .attr('id', `component-${componentName}`);

  d3.select(`#variable-svg`)
    .append('text')
    .attr('x', component_x)
    .attr('y', component_y)
    .attr('stroke', 'black')
    .style('font-size', 14)
    .style('text-anchor', 'middle')
    .text(componentName);
  return intervals;
};

export default draw;
