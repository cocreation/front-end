import React from 'react';
import { Grid, IconButton } from '@material-ui/core';
import SelectCard from '../../Components/LanguageCard';
import { ArrowBack } from '@material-ui/icons';
import VariableComponent from '../../Components/variable';
import draw from './draw';
import { getVariables, likeVariable, getComponentDetail } from '../../Utils/api';
import { withNamespaces } from 'react-i18next';

class SelectVariables extends React.Component {
  state = {
    variables: [],
    arrows: [],
  };

  componentDidMount() {
    const { componentId } = this.props.match.params;
    const { t } = this.props;
    this.setState({
      variables: [
        {
          id: 'variable_knowing',
          title: t('variable_knowing'),
          position: {
            col: 1,
            row: 2,
          },
        },
        {
          id: 'variable_destructive',
          title: t('variable_destructive'),
          position: {
            col: 2,
            row: 1,
          },
        },
        {
          id: 'variable_constructive',
          title: t('variable_constructive'),
          position: {
            col: 2,
            row: 3,
          },
        },
        {
          id: 'variable_resistance',
          title: t('variable_resistance'),
          position: {
            col: 3,
            row: 1,
          },
        },
        {
          id: 'variable_reinforce',
          title: t('variable_reinforce'),
          position: {
            col: 3,
            row: 3,
          },
        },
        {
          id: 'variable_protection',
          title: t('variable_protection'),
          position: {
            col: 4,
            row: 1,
          },
        },
        {
          id: 'variable_alternative',
          title: t('variable_alternative'),
          position: {
            col: 4,
            row: 3,
          },
        },
        {
          id: 'variable_healing',
          title: t('variable_healing'),
          position: {
            col: 5,
            row: 1,
          },
        },
        {
          id: 'variable_growth',
          title: t('variable_growth'),
          position: {
            col: 5,
            row: 3,
          },
        },
        {
          id: 'variable_vision',
          title: t('variable_vision'),
          position: {
            col: 6,
            row: 2,
          },
        },

      ],
      arrows: [
        { from: { row: 2, col: 1 }, to: { row: 1, col: 2 } },
        { from: { row: 2, col: 1 }, to: { row: 3, col: 2 } },
        { from: { row: 1, col: 2 }, to: { row: 1, col: 3 } },
        { from: { row: 1, col: 3 }, to: { row: 1, col: 4 } },
        { from: { row: 1, col: 4 }, to: { row: 1, col: 5 } },
        { from: { row: 1, col: 5 }, to: { row: 2, col: 6 } },
        { from: { row: 3, col: 2 }, to: { row: 3, col: 3 } },
        { from: { row: 3, col: 3 }, to: { row: 3, col: 4 } },
        { from: { row: 3, col: 4 }, to: { row: 3, col: 5 } },
        { from: { row: 3, col: 5 }, to: { row: 2, col: 6 } },
      ],
    }, () => {
      getComponentDetail(componentId)
        .then(response => {
          if (response.status === 200) {
            const intervals = draw(this.state.variables, this.state.arrows, response.data.data.title[this.props.lng] ? response.data.data.title[this.props.lng] : response.data.data.title[response.data.data.languages[0]], onVariableClick);
            this.setState({
              ...this.state,
              intervals,
            });
          }
        });
    });

    const onVariableClick = (variable) => () => {
      const { componentId } = this.props.match.params;
      this.props.history.push(`/main/component/${componentId}/${variable}/rooms`);
    };


    /*getVariables()
      .then(response => {
        if (response.status === 200) {
          this.setState({
            variables: response.data.data,
          });
        }
      });*/
  }

  componentWillUnmount() {
    console.log('Un-mounting');
    const { intervals } = this.state;
    intervals.forEach((i) => {
      clearInterval(i);
    });
  }

  handleOnItemClick = id => {
    this.props.onClickCallBack(this.state.variables.filter(i => i.title === id)[0]);
  };

  handleOnFollowItemClick = id => {
  };

  handleOnBackClick = () => {
    this.props.history.goBack();
  };

  /*handeleOnLikeClick = id => {
    likeVariable(id)
      .then(response => {
        if (response.status === 200) {
          this.setState(prevState => {
            return {
              ...prevState,
              variables: prevState.variables.map(c => {
                if (c.title === id) {
                  return {
                    ...c,
                    likes: c.likes + 1,
                  };
                } else {
                  return {
                    ...c,
                  };
                }
              }),
            };
          });
        }
      }).catch(error => {
      console.log(error);
    });
  };*/

  render() {
    const { variables } = this.state;
    return (
      <Grid container>
        <Grid item xs={12}>
          <IconButton onClick={this.handleOnBackClick}>
            <ArrowBack/>
          </IconButton>
        </Grid>
        <div id={'variables'} style={{
          height: '500px',
        }}/>
      </Grid>
    );
  }
}

export default withNamespaces('translation')(SelectVariables);
