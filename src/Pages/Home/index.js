import React, { Component } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CreateComponentPanel from '../../Components/Create-Component-Panel';
import { contributeToComponent, getAllComponents, getComponents, likeComponent } from '../../Utils/api';
import ComponentCard from '../../Components/ComponentCard';
import { withNamespaces } from 'react-i18next';

const styles = theme => ({
  relative: {
    position: 'relative',
  },
  fillBody: {
    height: '75vh',
  },
  scrollable: {
    overflowY: 'scroll',
  },
});

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      components: [],
    };
  }

  componentDidMount() {
    getAllComponents()
      .then(response => {
        if (response.status === 200) {
          this.setState({
            components: response.data.data,
          });
        }
      }).catch(error => {

    });
  }

  handleOnCreateNewComponent = (component) => {
    this.setState(prevState => ({
      ...prevState,
      components: [component, ...prevState.components],
    }));
  };

  handeleOnLikeClick = id => {
    likeComponent(id)
      .then(response => {
        if (response.status === 200) {
          this.setState(prevState => {
            return {
              ...prevState,
              components: prevState.components.map(c => {
                if (c._id === id) {
                  return {
                    ...c,
                    likes: c.likes + 1,
                  };
                } else {
                  return {
                    ...c,
                  };
                }
              }),
            };
          });
        }
      }).catch(error => {
      console.log(error);
    });
  };

  onComponentClick = (component) => {
    this.props.history.push(`/main/component/${component}`);
  };

  handleContribute = () => {
    const { componentToContribute, contributeTitle } = this.state;
    const { lng } = this.props;
    contributeToComponent(componentToContribute, contributeTitle, lng)
      .then(response => {
        if (response.status === 201) {
          this.setState({
            contributeOpen: false,
            contributeTitle: '',
            componentToContribute: null,
          }, () => {

            getAllComponents().then(resposne => {
              if (resposne.status === 200) {
                this.setState({
                  components: resposne.data.data,
                });
              }
            }).catch(error => {
              if (error.response) {
                const serverError = error.response.data.data;
                //this.props.errorHandler(new Error(serverError.message));
              }
            });
          });
        }
      }).catch(error => {
      if (error.response) {
        const serverError = error.response.data;
        //this.props.errorHandler(new Error(serverError.message));
      }
    });
  };

  render() {
    console.log(this.props);
    const { components, contributeOpen, contributeTitle } = this.state;
    const { classes, t, contentLanguage } = this.props;
    return (
      <Grid container alignContent={'center'}
            alignItems={'center'} justify={'center'}>


        <Grid id={'content'} item sm={12}
        >
          <CreateComponentPanel handleOnCreateNewComponent={this.handleOnCreateNewComponent}/>
          <div className={`${classes.fillBody} ${classes.scrollable}`}>
            {components.map(component => (
              <ComponentCard
                title={component.title}
                id={component._id}
                categories={component.categories}
                contentLanguage={this.props.lng}
                languages={component.languages}
                likes={component.likes}
                onLikeClick={this.handeleOnLikeClick}
                onFollowClick={() => {
                }}
                onValueClick={() => {
                }}
                onCoCreationClick={() => {
                }}
                onContributeClick={(id) => this.setState({ contributeOpen: true, componentToContribute: id })}
                onClickCallBack={this.onComponentClick}/>
            ))}
          </div>
        </Grid>
        <Dialog open={contributeOpen}
                onBackdropClick={() => this.setState({
                  contributeOpen: false,
                  contributeTitle: '',
                  componentToContribute: null,
                })}>
          <DialogTitle>{t('contribute_title')}</DialogTitle>
          <DialogContent>
            <TextField fullWidth style={{ margin: 4 }} label={t('component_title')} variant={'outlined'}
                       value={contributeTitle} onChange={event => this.setState({
              contributeTitle: event.target.value,
            })}/>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleContribute}>{t('submit')}</Button>
            <Button onClick={() => this.setState({
              contributeOpen: false,
              contributeTitle: '',
              componentToContribute: null,
            })}>{t('cancel')}</Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
  }
}


export default React.memo(withNamespaces('translation')(withStyles(styles)(Home)), (prevProps, nextProps) => (
  prevProps.contentLanguage === nextProps.contentLanguage
));
