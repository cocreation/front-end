import React from 'react';
import {
  Button,
  Grid,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { withNamespaces } from 'react-i18next';
import i18n from '../../i18n';

const SelectLanguageStyles = theme => ({
  wrapPage: {
    width: '100%',
    height: '100%',
  },
  selectButtons: {
    height: 80,
    width: '100%',
    marginTop: theme.spacing(2),
  },
  input: {
    marginTop: theme.spacing(2),
  },
  languageList: {
    height: 400,
    width: '50%',
    overflowY: 'scroll',
    display: 'flex',
    flexDirection: 'column',
    '-ms-overflow-style': 'none',
  },
});

class SelectLanguagePage extends React.Component {
  state = {
    language: 'en',
    wantToRequestLanguage: false,
    languageList: [
      { label: 'English', code: 'en' },
      { label: 'German', code: 'de' },
      { label: 'Persian', code: 'fa' },
    ],
  };

  handleSelectLanguage = lang => () => {
    this.setState(
      {
        language: lang,
      },
      () => {
        this.props.languageChanger(this.state.language);
        i18n.changeLanguage(this.state.language);
        this.props.history.goBack();
      },
    );
  };

  handleOpenRequestLanguage = () => {
    this.setState({
      wantToRequestLanguage: true,
    });
  };

  handleCloseRequestLanguage = () => {
    this.setState({
      wantToRequestLanguage: false,
    });
  };

  render() {
    const { classes, t, history } = this.props;
    const { language, wantToRequestLanguage, languageList } = this.state;
    return (
      <Grid
        container
        justify={'space-around'}
        alignContent={'center'}
        direction={'column'}
        className={classes.wrapPage}
      >
        <Typography variant={'h5'} align={'center'}>
          {t('choosen_language')}
          {language}
        </Typography>
        <TextField variant={'outlined'} label={t('search_language')} />
        <div className={classes.languageList}>
          {languageList.map(lan => (
            <Button
              key={lan.code}
              variant={'outlined'}
              onClick={this.handleSelectLanguage(lan.code)}
              className={classes.selectButtons}
              fullWidth
            >
              {lan.label}
            </Button>
          ))}
        </div>

        <Button variant={'text'} onClick={this.handleOpenRequestLanguage}>
          {t('request_another_language')}
        </Button>
        <Button onClick={() => history.push('/')}>{t('back_to_home')}</Button>
        <Dialog open={wantToRequestLanguage} onClose={this.handleCloseRequestLanguage}>
          <DialogTitle>Please fill the form...</DialogTitle>
          <DialogContent>
            <TextField
              variant={'outlined'}
              fullWidth
              label={'Languag Name:'}
              className={classes.input}
            />
            <TextField
              variant={'outlined'}
              fullWidth
              label={'Languag Description:'}
              className={classes.input}
            />
          </DialogContent>
          <DialogActions>
            <Button
              color={'secondary'}
              variant={'outlined'}
              onClick={this.handleCloseRequestLanguage}
            >
              Cancel!
            </Button>
            <Button color={'primary'} variant={'contained'}>
              Send Request!
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    );
  }
}

export default withNamespaces('translation')(withStyles(SelectLanguageStyles)(SelectLanguagePage));
