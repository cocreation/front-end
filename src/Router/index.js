import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import SelectLanguagePage from '../Pages/Select-Language';
import MainPage from '../Pages/Main';
import Home from '../Pages/Home';
import ComponentDetails from '../Pages/Component-Details';
import RoomDetails from '../Pages/Room-Detail';
import SelectVariables from '../Pages/Select-variables';
import CreateRoom from '../Pages/Create-Room';
import CreatePostDialog from '../Components/Create-Post-Dialog';

export default props => (
  <Switch>
    <Route path="/" exact render={() => <Redirect to="/main"/>}/>
    <Route exact path="/main" render={(props) => (<Home  {...props} contentLanguage={props.contentLanguage}/>)}/>
    <Route path="/main/component/:componentId" exact
           render={(props) => (<SelectVariables {...props} contentLanguage={props.contentLanguage}/>)}/>
    <Route path="/main/component/:componentId/:variableId/rooms"
           render={(props) => (<CreateRoom {...props} contentLanguage={props.contentLanguage}/>)}/>
    <Route path="/main/component/:componentId/:variableId/createPost"
           render={(props) => (<CreatePostDialog {...props} contentLanguage={props.contentLanguage}/>)}/>
    <Route path="/main/room/:roomId"
           render={(props) => (<RoomDetails {...props} contentLanguage={props.contentLanguage}/>)}/>
    <Route
      path="/settings/language"
      render={({ history }) => (
        <SelectLanguagePage history={history} languageChanger={props.languageChanger}/>
      )}
    />
  </Switch>
);
