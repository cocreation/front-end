import React from 'react';
import {withNamespaces} from 'react-i18next';
import {
    Card,
    CardActions,
    CardContent,
    CardActionArea,
    Typography,
    Button,
    IconButton,
    Chip,
} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import {Favorite} from '@material-ui/icons';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';

const LanguageCardStyle = theme => ({
    root: {
        width: '100%',
        backgroundColor: '#f2f2f2',
        marginTop: theme.spacing(4),
        padding: theme.spacing(4)
    },
    content: {
        height: 140,
    },
});

export default withNamespaces('translation')(
    withStyles(LanguageCardStyle)(
        ({
             onClickCallBack,
             classes,
             title,
             languages,
             contentLanguage,
             onContributeClick,
             onLikeClick,
             likes,
             categories,
             id,
             t,
         }) => {
            return (
                <React.Fragment>
                    <Grid container className={classes.root}  direction="row"
                          justify="space-around"
                          spacing={2}
                          alignItems="center" >
                        {title[contentLanguage] ? null : (
                            <Grid item sm={4} >
                                <Button variant={'outlined'} onClick={() => onContributeClick(id)}>
                                    {t('start_in_another_language')}
                                </Button>
                            </Grid>

                        )}
                        <Grid item sm={title[contentLanguage] ? 6 : 4} onClick={() => onClickCallBack(id)} >
                            <Typography color={'primary'}
                                variant={'h4'}>{title[contentLanguage] ? title[contentLanguage] : title[languages[0]]}</Typography>
                        </Grid>
                        <Grid item sm={title[contentLanguage] ? 6 : 4}  onClick={() => onClickCallBack(id)}>
                            <Typography>{t('this_component_created_in')}:</Typography>
                            <ul>
                                {languages.map(l => (
                                    <li>{l}</li>
                                ))}
                            </ul>
                        </Grid>

                    </Grid>
                    {categories ? categories.map(category => (
                        <Chip style={{margin: 8}} size={'small'}
                              label={`${t(category.categoryA.title)}${category.categoryB ? '/' + t(category.categoryB.title) : ''}`}/>
                    )) : null}
                </React.Fragment>
            );
        },
    ),
);
