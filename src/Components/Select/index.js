import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withNamespaces } from 'react-i18next';

class CoSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
    };
  }

  handleOnChange = (event) => {
    this.setState({
      value: event.target.value,
    }, () => {
      const { list } = this.props;
      this.props.onItemSelect(list.find(item => item._id === this.state.value || item.title === this.state.value));
    });
  };

  render() {
    const { value } = this.state;
    const { list, label, t, enabled } = this.props;
    return (
      <FormControl variant={'outlined'} fullWidth style={{ margin: 4 }} disabled={!enabled}>
        <InputLabel id="demo-simple-select-filled-label">{label}</InputLabel>
        <Select value={value} onChange={this.handleOnChange} displayEmpty>
          {list.map(item => (
            <MenuItem key={item._id} value={item._id || item.title}>{t(item.title)}</MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  }
}

CoSelect.propTypes = {
  list: PropTypes.array.isRequired,
  onItemSelect: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  enabled: PropTypes.bool,
  selected: PropTypes.object.isRequired,

};

CoSelect.defaultProps = {
  enabled: true,
};

export default withNamespaces('translation')(CoSelect);
