import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button, Typography } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';

const styles = theme => ({
  logo: {
    position: 'fixed',
    bottom: 10,
    left: 10,
    width: 200,
  },
  item: {
    marginTop: theme.spacing(1),
  },
});

export default withNamespaces('translation')(
  withStyles(styles)(({ classes, t }) => {
    return (
      <React.Fragment>
        <Typography variant={'h5'} align={'center'}>
          {t('collective_reality')}
        </Typography>
        <Button fullWidth className={classes.item}>
          Category A
        </Button>
        <Button fullWidth className={classes.item}>
          Category B
        </Button>
        <Button fullWidth className={classes.item}>
          Components
        </Button>
        <Button fullWidth className={classes.item}>
          Is-condition
        </Button>
        <Button fullWidth className={classes.item}>
          Is-Ambition
        </Button>
        <Button fullWidth className={classes.item}>
          Should-condition
        </Button>
        <Button fullWidth className={classes.item}>
          Should-Ambition
        </Button>
        <Button fullWidth className={classes.item}>
          Options
        </Button>
        <Button fullWidth className={classes.item}>
          Articel
        </Button>
        <Button fullWidth className={classes.item}>
          News
        </Button>

        <img className={classes.logo} src="/images/logo.png" />
      </React.Fragment>
    );
  }),
);
