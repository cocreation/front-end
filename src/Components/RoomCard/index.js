import React from 'react';
import { withNamespaces } from 'react-i18next';
import {
  Card,
  CardActions,
  CardContent,
  CardActionArea,
  Typography,
  Button,
  IconButton,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Favorite } from '@material-ui/icons';

const LanguageCardStyle = theme => ({
  root: {
    width: '100%',
    backgroundColor: '#f2f2f2',
    marginTop: theme.spacing(4),
  },
  content: {
    height: 140,
  },
});

export default withNamespaces('translation')(
  withStyles(LanguageCardStyle)(
    ({
       onClickCallBack,
       room,
       classes,
       title,
       onFollowClick,
       onValueClick,
       onCoCreationClick,
       id,
       t,
       likes,
       onLikeClick,
     }) => {
      return (
        <Card className={classes.root}>
          <CardActionArea className={classes.content} onClick={() => onClickCallBack(room)}>
            <CardContent>
              <Typography variant={'h4'}>{t(title)}</Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <IconButton onClick={() => onLikeClick(id)}>
              <Favorite/>{likes}
            </IconButton>
            <Button variant={'text'} onClick={() => onValueClick(id)}>
              {t('join')}
            </Button>
            <Button variant={'text'} onClick={() => onCoCreationClick(id)}>
              {t('report')}
            </Button>
          </CardActions>
        </Card>
      );
    },
  ),
);
