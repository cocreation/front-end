import React from 'react';
import { Typography, Card} from "@material-ui/core";

export default ({message}) => {
    return (
        <Card variant={"elevation"} style={{margin: 4, padding: 4}}>
            <Typography>{message.message}</Typography>
        </Card>
    )
}