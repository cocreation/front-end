import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {withNamespaces} from 'react-i18next';
import Chip from '@material-ui/core/Chip';
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
    root: {
        width: '95%',
        margin: 2
    },
    media: {
        height: 190,
    },
});

const PostCard = (props) => {
    const classes = useStyles();
    const {t} = props;

    return (
        <Grid item sm={12} justify={'center'}>
            <Card className={classes.root}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={`${process.env.REACT_APP_API_URL}/static/images/${props.image}`}
                        title={props.title}

                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {props.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {props.content}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Chip label={t(props.room)}/>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default withNamespaces('translation')(PostCard);