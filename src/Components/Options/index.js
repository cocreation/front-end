import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button, Typography } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';
const styles = theme => ({
  logo: {
    position: 'fixed',
    bottom: 10,
    left: 10,
    width: 200,
  },
  item: {
    marginTop: theme.spacing(1),
  },
});

export default withNamespaces('translation')(
  withStyles(styles)(({ classes, t }) => {
    return (
      <React.Fragment>
        <Typography variant={'h5'} align={'center'}>
          {t('options')}
        </Typography>
        <Button fullWidth className={classes.item}>
          Community
        </Button>
        <Button fullWidth className={classes.item}>
          Network
        </Button>
        <Button fullWidth className={classes.item}>
          Bibliotheca
        </Button>
        <Button fullWidth className={classes.item}>
          Instances
        </Button>
        <Button fullWidth className={classes.item}>
          Proavtve
        </Button>
        <Button fullWidth className={classes.item}>
          Events
        </Button>
        <Button fullWidth className={classes.item}>
          Offers
        </Button>
        <Button fullWidth className={classes.item}>
          Survey
        </Button>
        <Button fullWidth className={classes.item}>
          Competences
        </Button>
        <Button fullWidth className={classes.item}>
          Resource
        </Button>
        <Button fullWidth className={classes.item}>
          Finance
        </Button>
        <Button fullWidth className={classes.item}>
          Coworking
        </Button>
        <Button fullWidth className={classes.item}>
          Map
        </Button>
      </React.Fragment>
    );
  }),
);
