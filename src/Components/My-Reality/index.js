import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button, Typography } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';

const styles = theme => ({
  logo: {
    position: 'fixed',
    bottom: 10,
    left: 10,
    width: 200,
  },
  item: {
    marginTop: theme.spacing(1),
  },
});

export default withNamespaces('translation')(
  withStyles(styles)(({ classes, t }) => {
    return (
      <React.Fragment>
        <Typography variant={'h5'} align={'center'}>
          {t('my_reality')}
        </Typography>
        <Button fullWidth className={classes.item}>
          My Components
        </Button>
        <Button fullWidth className={classes.item}>
          My Options
        </Button>
        <Button fullWidth className={classes.item}>
          My articel
        </Button>
        <Button fullWidth className={classes.item}>
          My Friends
        </Button>
        <Button fullWidth className={classes.item}>
          Followed Categorie A
        </Button>
        <Button fullWidth className={classes.item}>
          Followed Categorie B
        </Button>
        <Button fullWidth className={classes.item}>
          Followed Components
        </Button>
        <Button fullWidth className={classes.item}>
          Followed Variable
        </Button>
        <Button fullWidth className={classes.item}>
          Followed Otions
        </Button>
        <Button fullWidth className={classes.item}>
          Followed articel
        </Button>
        <Button fullWidth className={classes.item}>
          News
        </Button>
      </React.Fragment>
    );
  }),
);
