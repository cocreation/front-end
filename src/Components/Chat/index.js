import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Send from '@material-ui/icons/Send';
import TextField from "@material-ui/core/TextField";
import Message from '../Message';
import {withNamespaces} from 'react-i18next';
import socketIOClient from "socket.io-client";
import {withStyles} from "@material-ui/styles";
import axios from 'axios';

const styles = theme => ({
    relative: {
        position: 'relative',
    },
    conversation: {
        height: '60vh',
    },
    fillBody: {
        height: '75vh',
    },
    scrollable: {
        overflowY: 'scroll',
    },
    filterItem: {
        width: '100%',
        margin: theme.spacing(0.2),
    },
    fullWidthCol: {
        width: '100%',
    },
    filterActive: {
        backgroundColor: 'grey',
    },
    fab: {
        position: 'fixed',
        bottom: 30,
        right: 30,
    },
    flex: {
        flex: 1,
        margin: theme.spacing(1)
    }
});

class Chat extends Component {
    state = {
        messages: [],
        message: ''
    }
    CHAT_BASE_URL = process.env.REACT_APP_CHAT_URL
    socket = null

    componentDidMount() {
        const {roomId} = this.props;
        if (roomId) {
            axios.get(`${this.CHAT_BASE_URL}/messages/${roomId}`)
                .then(response => {
                    this.setState({
                        messages: response.data.data
                    })
                });
            this.socket = socketIOClient(`${this.CHAT_BASE_URL}/chat`);
            this.socket.on(roomId, data => {
                console.log(data)
                this.setState(prevState => {
                    return {
                        ...prevState,
                        messages: [...prevState.messages, data]
                    }
                });
            });
        }
    }

    sendMessage = () => {
        const {roomId} = this.props;
        const {message} = this.state;
        this.socket.emit('sendMessage', {roomId, message})
        this.setState({message: ''})
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.roomId !== this.props.roomId) {
            if (this.props.roomId) {
                axios.get(`${this.CHAT_BASE_URL}/messages/${this.props.roomId}`)
                    .then(response => {
                        this.setState({
                            messages: response.data.data
                        })
                    });
                this.socket = socketIOClient(`${this.CHAT_BASE_URL}/chat`);
                this.socket.on(this.props.roomId, data => {
                    console.log(data)
                    this.setState(prevState => {
                        return {
                            ...prevState,
                            messages: [...prevState.messages, data]
                        }
                    });
                });
            }
        }
    }

    render() {
        const {roomId, t, classes} = this.props;
        const {messages, message} = this.state;
        if (roomId) {
            return (
                <Grid container direction={'column'}>
                    <div className={`${classes.conversation} ${classes.scrollable}`}>
                        {messages.map(msg => (
                            <Message key={msg._id} message={msg}/>
                        ))}
                    </div>
                    <Grid container direction={"row"}>
                        <TextField variant={"outlined"} className={classes.flex} label={'Type Message...'}
                                   value={message} onChange={event => this.setState({message: event.target.value})}
                                   onKeyPress={(event) => {
                                       if (event.key === 'Enter') {
                                           this.sendMessage()
                                       }
                                   }}/>
                        <IconButton onClick={this.sendMessage}>
                            <Send/>
                        </IconButton>
                    </Grid>

                </Grid>
            )
        } else {
            return (
                <Typography>{t('select_room_for_chat')}</Typography>
            );
        }
    }
}

Chat.propTypes = {
    roomId: PropTypes.string
};

export default withStyles(styles)(withNamespaces('translation')(Chat));