import * as d3 from 'd3';

const draw = (props) => {
  d3.select('.variable > *').remove();


  // compute window size
  const width = Math.max(document.documentElement.clientWidth, window.innerWidth) || 0;
  const height = 100;//Math.max(document.documentElement.clientHeight, window.innerHeight) || 0;
  console.log(Math.max(document.documentElement.clientHeight, window.innerHeight));
  // create svg container
  d3.select(`#variable-${props.id}`).append('svg')
    .attr('height', height)
    .attr('width', width)
    .attr('id', `variable-svg-${props.id}`);

  d3.select(`#variable-svg-${props.id}`)
    .append('ellipse')
    .attr('stroke', 'blue')
    .attr('stroke-width', '2px')
    .attr('fill', 'none')
    .attr('cx', 100)
    .attr('cy', 50)
    .attr('rx', 80)
    .attr('ry', 40);

  d3.select(`#variable-svg-${props.id}`)
    .append('text')
    .attr('x', 100)
    .attr('y', 50)
    .attr('stroke', 'black')
    .style('font-size', 19)
    .style('text-anchor', 'middle')
    .text(props.title);
};

export default draw;
