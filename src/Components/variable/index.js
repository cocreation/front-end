import React from 'react';
import draw from './draw'
import rd3 from 'react-d3-library';
import { palette, spacing,height, width } from '@material-ui/system';
import {Box} from '@material-ui/core'
export default class Variable extends React.Component {

  state = {
    d3: ''
  }
  componentDidMount(){
    draw(this.props)
  }

  render (){
    console.log(this.props);
    return (
      <Box id={`variable-${this.props.id}`} onClick={() => {
        console.log(this.props);
      }}/>
    );
  }
};
