import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles, Typography, Button } from '@material-ui/core';
import { withNamespaces } from 'react-i18next';

const useStyle = makeStyles(theme => ({
  footerContainer: {
    height: 35,
    width: '100%',
    backgroundColor: theme.palette.primary.main,
    position: 'fixed',
    bottom: 0,
    flexDirection: 'row',
    display: 'flex',
  },
  item: {
    flexGrow: 1,
    flex: 1,
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    textDecoration: 'none',
    color: theme.palette.text.primary,
  },
  text: {
    flexGrow: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
  },
}));

export default withNamespaces('translation')(
  withRouter(({ history, t }) => {
    const classes = useStyle();
    return (
      <div className={classes.footerContainer}>
        <Button variant="text" className={classes.item}>
          {t('imprint')}
        </Button>
        <Button variant="text" className={classes.item}>
          {t('security')}
        </Button>
        <Button
          variant="text"
          className={classes.item}
          onClick={() => history.push('/settings/language')}
        >
          <Typography variant={'subtitle'} className={classes.text}>
            {t('language')}
          </Typography>
        </Button>

        <Button variant="text" className={classes.item}>
          <Typography variant={'subtitle'} className={classes.text}>
            {t('mission')}
          </Typography>
        </Button>

        <Button variant="text" className={classes.item}>
          <Typography variant={'subtitle'} className={classes.text}>
            {t('contact')}
          </Typography>
        </Button>

        <Button variant="text" className={classes.item}>
          {t('co_creation')}
        </Button>

        <Button variant="text" className={classes.item}>
          <Typography variant={'subtitle'} className={classes.text}>
            {t('report_error')}
          </Typography>
        </Button>
      </div>
    );
  }),
);
