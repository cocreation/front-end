import * as  d3 from 'd3';

const node = document.createElement('div');

const width = 200,
  height = 100;
const svg = d3.select(node).append('svg')
  .attr('width', width)
  .attr('height', height)
  .style('margin', '2px');


svg.append('ellipse')
  .attr('stroke', 'blue')
  .attr('stroke-width', '2px')
  .attr('fill', 'none')
  .attr('cx', 100)
  .attr('cy', 50)
  .attr('rx', 80)
  .attr('ry', 40);

svg.append('text')
  .attr('x', 100)
  .attr('y', 50)
  .attr('stroke', 'black')
  .style('font-size', 19)
  .style("text-anchor", "middle")
  .text('Hello');


export default node;
