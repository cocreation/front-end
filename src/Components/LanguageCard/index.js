import React, { useEffect, useState } from 'react';
import { withNamespaces } from 'react-i18next';
import {
  Card,
  CardActions,
  CardContent,
  CardActionArea,
  Typography,
  Button,
  IconButton,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Favorite } from '@material-ui/icons';

const LanguageCardStyle = theme => ({
  root: {
    width: '100%',
    backgroundColor: '#f2f2f2',
    margin: theme.spacing(4),
  },
  content: {
    height: 140,
  },
});

export default withNamespaces('translation')(
  withStyles(LanguageCardStyle)(({ onClickCallBack, classes, title, onLikeClick, id, t , likes}) => {
    return (
      <Card className={classes.root}>
        <CardActionArea className={classes.content} onClick={() => onClickCallBack(id)}>
          <CardContent>
            <Typography variant={'h4'}>{t(title)}</Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <IconButton onClick={() => onLikeClick(id)}>
            <Favorite />{likes}
          </IconButton>
        </CardActions>
      </Card>
    );
  }),
);
