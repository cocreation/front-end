import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {withNamespaces} from 'react-i18next';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import {getRooms, createPost} from '../../Utils/api';
import CoSelect from '../Select';

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    field: {
        margin: theme.spacing(1),
    },
    fieldMultiLine: {
        height: 250,
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const CreatePostDialog = (props) => {
    const classes = useStyles();
    const {t} = props;
    const {componentId, variableId} = props.match.params;
    const [postTitle, setPostTitle] = useState('');
    const [saving, setSaving] = useState(false);
    const [category, setCategory] = useState('');
    const [postContent, setPostContent] = useState('');
    const [postImage, setPostImage] = useState(undefined);
    const [room, setRoom] = useState(undefined);
    const [rooms, setRooms] = useState([]);
    useEffect(() => {
        if (rooms.length === 0) {
            getRooms(componentId, variableId)
                .then(response => {
                    setRooms(response.data.data);
                }).catch(err => {
                console.log(err);
            });
        }
    }, [rooms]);
    useEffect(() => {
        console.log(postImage);
    }, [postImage]);

    const handleClose = () => {
        props.history.goBack();
    };

    const handleOnInputChange = (field) => (event) => {
        switch (field) {
            case 'title': {
                setPostTitle(event.target.value);
                return;
            }
            case 'content': {
                setPostContent(event.target.value);
                return;
            }
            case 'image': {
                setPostImage(event.target.files[0]);
                return;
            }
        }
    };
    const handleOnRoomSelect = (room) => {
        setRoom(room);
    };
    const handleCategorySelect = (category) => {
        setCategory(category.title)
    }

    const handleSavePost = () => {
        if (postTitle.length === 0) {
            return;
        }
        if (postContent.length === 0) {
            return;
        }
        if (room === undefined) {
            return;
        }
        setSaving(true);
        createPost(room._id, postTitle, postContent, postImage, category)
            .then(response => {
                props.history.goBack();
            }).catch(err => {
            console.log(err);
        }).finally(() => {
            setSaving(false);
        });
    };

    return (
        <Dialog open onClose={handleClose} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        {t('create_post')}
                    </Typography>
                    <Button autoFocus color="inherit" onClick={handleSavePost}>
                        {t('save')}
                    </Button>
                </Toolbar>
                {saving ? (<LinearProgress/>) : null}
            </AppBar>
            <Grid container>
                <TextField fullWidth label={'Image'} className={classes.field} type={'file'} multi={false}
                           rows={10}
                           disabled={saving}
                           onChange={handleOnInputChange('image')}
                           variant={'outlined'}/>
                <CoSelect className={classes.field} label={'Select Room'} list={rooms}
                          enabled={!saving}
                          onItemSelect={handleOnRoomSelect}/>
                <CoSelect className={classes.field} label={'Select Category'} list={[
                    'Inform',
                    'Community',
                    'Instances',
                    'Initiatives',
                    'Events',
                    'Offer',
                    'Survey',
                    'Jobs',
                    'Source funding',
                    'Crowdfunding',
                    'Coworking'
                ].map(t => ({title: t, _id: t}))}
                          enabled={!saving}
                          onItemSelect={handleCategorySelect}/>
                <TextField fullWidth value={postTitle} label={'Post Title'} className={classes.field}
                           variant={'outlined'}
                           disabled={saving}
                           onChange={handleOnInputChange('title')}/>
                <TextField fullWidth value={postContent} label={'Post Content'}
                           disabled={saving}
                           className={`${classes.field} ${classes.fieldMultiLine}`} type={'textarea'} multiline
                           onChange={handleOnInputChange('content')}
                           variant={'outlined'}/>

            </Grid>
        </Dialog>
    );
};

export default withNamespaces('translation')(CreatePostDialog);