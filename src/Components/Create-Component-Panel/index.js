import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import { createCompoent, getCategoryAList, getCategoryBList, getVariables, getAllCategoryBList } from '../../Utils/api';
import CoSelect from '../Select';
import { withNamespaces } from 'react-i18next';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import { Add } from '@material-ui/icons';


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',

  },
  column: {
    flexBasis: '33.33%',
  },
  helper: {
    padding: theme.spacing(1, 2),
  },
  link: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
  select: {
    margin: theme.spacing(1),
  },
}));

export default withNamespaces('translation')(function CreateComponentPanel({ t, handleOnCreateNewComponent , ...props}) {
  const classes = useStyles();
  const [componentTitle, setComponentTitle] = useState('');
  const [expanded, setExpanded] = useState(false);
  const [categoryAList, setCategoryAList] = useState([]);
  const [categoryBList, setCategoryBList] = useState([]);
  const [languagesList, setLanguageList] = useState([{ title: 'en' }, { title: 'fa' }, { title: 'de' }]);
  const [categories, setCategories] = useState([{ categoryA: null, categoryB: null, bList: [] }]);
  const [selectedCategoryA, setCategoryA] = useState(null);
  const [selectedCategoryB, setCategoryB] = useState(null);
  const [selectedLanguage, setLanguage] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [fetching, setFetching] = useState(false);
  useEffect(() => {
    if (expanded) {
      getCategoryAList().then(response => {
        if (response.status === 200) {
          setCategoryAList(response.data.data);
        }
      });
      getAllCategoryBList().then(response => {
        if (response.status === 200) {
          setCategoryBList(response.data.data);
        }
      });
    } else {
      setComponentTitle('');
      setCategoryB(null);
      setCategoryA(null);
    }
  }, [expanded]);

  // useEffect(() => {
  //   categories.forEach((category, index) => {
  //     if (category.categoryA !== null && category.categoryB === null) {
  //       getCategoryBList(category.categoryA._id)
  //         .then(response => {
  //           if (response.status === 200) {
  //             const c = categories.map((cat, i) => {
  //               if (i !== index) {
  //                 return cat;
  //               } else {
  //                 return {
  //                   ...cat,
  //                   bList: response.data.data,
  //                 };
  //               }
  //             });
  //             setCategories(c);
  //           }
  //         });
  //     }
  //   });
  // }, [categories]);

  function onPanelChange() {
    setCategories([{categoryA: null, categoryB: null}]);
    setComponentTitle('');
    setExpanded(!expanded);
  }

  function handleOnTitleChange(event) {
    setComponentTitle(event.target.value);
  }


  function onLanguageSelect(language) {
    setLanguage(language);
  }

  function fetchBList(index, category) {

    getCategoryBList(category._id)
      .then(response => {
        if (response.status === 200) {
          const { data } = response.data;
          const newState = categories.map(cat => {
            if (cat._id === category._id) {
              return {
                ...cat,
                bList: data,
              };
            } else {
              return {
                ...cat,
              };
            }
          });

          setCategories(newState);
        }
      });
  }

  function onCategorySelect(categoryName, index) {
    return function(category) {
      if (categoryName === 'category-a') {
        const c = categories.map((cat, i) => {
          if (i !== index) {
            return cat;
          } else {
            return {
              ...cat,
              categoryA: category,
            };
          }
        });
        setCategories(c);
        //fetchBList(index, category);
      } else if (categoryName === 'category-b') {
        const c = categories.map((cat, i) => {
          if (i !== index) {
            return cat;
          } else {
            return {
              ...cat,
              categoryB: category,
            };
          }
        });
        setCategories(c);
      }
    };
  }

  function onSubmitClick() {
    if (fetching) {
      setErrorMsg('You Cannot Send Multiple Request!');
    }
    if (categories.length === 1 && categories[0].categoryA === null) {
      setErrorMsg('Select A Category');
      return;
    }
    if (componentTitle.trim().length !== 0) {
      setFetching(true);
      createCompoent({
        title: componentTitle.trim(),
        categories,
        contentLanguage: props.lng,
      }).then(response => {
        setFetching(false);
        if (response.status === 201) {
          setErrorMsg('Component Created Successfully');
          handleOnCreateNewComponent(response.data.data);
          onPanelChange();
        }
      }).catch(error => {
        setFetching(false);
        if (error.response) {
          const serverError = error.response.data.data;
          setErrorMsg(serverError.message);
        }
      });
    } else {
      setErrorMsg('Please Enter the title of component');
    }
  }

  return (
    <div className={classes.root}>
      <ExpansionPanel onChange={onPanelChange} expanded={expanded}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon/>}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          <Typography className={classes.heading}>{t('create_component_title')}</Typography>

        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.details}>
          <Grid container spacing={1}>
            <Grid item md={12}>
              <TextField fullWidth variant={'outlined'} value={componentTitle} onChange={handleOnTitleChange}
                         multiline
                         helperText={'Type the thing, you would transform...'}
                         label={t('component_title')}/>
            </Grid>
            {categories.map((category, index) => (
              <React.Fragment>
                <Grid item sm={5}>
                  <CoSelect label={'Category A'} list={categoryAList}
                            onItemSelect={onCategorySelect('category-a', index)}/>

                </Grid>
                <Grid item sm={5}>
                  <CoSelect label={'Category B'} list={category.categoryA ? categoryBList.filter(c => c.parentCategoryA._id === category.categoryA._id) : []}
                            onItemSelect={onCategorySelect('category-b', index)}
                            enabled={category.categoryA !== null}/>
                </Grid>
              </React.Fragment>
            ))}


            <Grid item sm={2}>
              <IconButton
                onClick={() => setCategories([...categories, { categoryA: null, categoryB: null, bList: [] }])}>
                <Add/>
              </IconButton>

            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <Divider/>
        <ExpansionPanelActions>
          {fetching ? (
            <CircularProgress size={14}/>
          ) : null}
          <Button size="small" onClick={onPanelChange}>{t('cancel')}</Button>
          <Button size="small" color="primary" onClick={onSubmitClick}>
            {t('submit')}
          </Button>

        </ExpansionPanelActions>
      </ExpansionPanel>
      <Snackbar open={errorMsg !== null} message={errorMsg} autoHideDuration={3000} onClose={() => setErrorMsg(null)}/>
    </div>
  );
});
