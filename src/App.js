import React, { useState, useEffect } from 'react';
import { Grid, IconButton } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ApplicationRouter from './Router';
import Footer from './Components/footer';
import './App.css';
import { withNamespaces } from 'react-i18next';
import './i18n';
import RTL from './RTL';
import Home from './Pages/Home';
import { Language } from '@material-ui/icons';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router-dom';

const AppStyles = theme => ({
  header: {
    width: '100%',
    height: '7vh',
    background: theme.palette.primary.main,
  },
  container: {
    width: '100%',
    height: '88vh',
  },
  footer: {
    width: '100%',
    height: '5vh',
    background: theme.palette.primary.main,
  },
});

class App extends React.Component {
  state = {
    language: 'en',
    selectContentLanguageOpen: false,
  };

  setLanguage = lan => {
    this.setState({
      language: lan,
      contentLanguage: 'en'
    });
  };

  handleOpenSelectLanguage = () => {
    this.setState({
      selectContentLanguageOpen: true,
    });
  };


  handleCloseSelectLanguage = () => {
    this.setState({
      selectContentLanguageOpen: false,
    });
  };


  handleSelectContentLanguage = (lan) => () => {
    this.setState({ contentLanguage: lan, selectContentLanguageOpen: false });
  };


  render() {
    console.log('juuuu',this.props);
    const { language, selectContentLanguageOpen,contentLanguage } = this.state;
    const { t, classes } = this.props;
    if (language === 'fa') {
      return (
        <RTL>
          <Grid container dir="rtl">
            <Grid id="header" item xs={12} className={classes.header}/>
            <Grid id="container" item xs={12} className={classes.container}>
              <Grid container direction={'row'}>
                <Grid id={'user'} item sm={false} md={3}/>
                <Grid id={'content'} item sm={12}>
                  <Grid item xs={12}>
                    <IconButton onClick={() => this.props.history.push('/settings/language')}>
                      <Language/>
                    </IconButton>
                  </Grid>
                  <ApplicationRouter languageChanger={this.setLanguage} contentLanguage={contentLanguage}/>
                </Grid>
                <Grid id={'navigation'} item sm={false} md={3}/>
              </Grid>

            </Grid>
            <Grid id="footer-container" item xs={12} className={classes.footer}>
              <Footer/>
            </Grid>
          </Grid>
          <Dialog open={selectContentLanguageOpen} onBackdropClick={this.handleCloseSelectLanguage}>
            <DialogTitle>
              <Typography>{t('choose_language')}</Typography>
            </DialogTitle>
            <DialogContent>
              {[{ key: 'lan_en', language: 'en' }, { key: 'lan_de', language: 'de' }, { key: 'lan_fa', language: 'fa' }]
                .map(lan => (
                  <Button variant={'text'} fullWidth
                          onClick={this.handleSelectContentLanguage(lan.language)}>{t(lan.key)}</Button>
                ))}

            </DialogContent>
          </Dialog>
        </RTL>
      );
    } else {
      return (
        <Grid container dir="ltr">
          <Grid id="header" item xs={12}  className={classes.header}/>
          <Grid id="container" item xs={12} className={classes.container}>
            <Grid container direction={'row'}>
              <Grid id={'user'} item sm={false} md={3}  hidden/>
              <Grid id={'content'} item sm={12}>
                <Grid item xs={12}>
                  <IconButton onClick={() => this.props.history.push('/settings/language')}>
                    <Language/>
                  </IconButton>
                </Grid>
                <ApplicationRouter languageChanger={this.setLanguage} contentLanguage={contentLanguage}/>
              </Grid>
              <Grid id={'navigation'} item sm={false} hidden md={3}/>
            </Grid>
          </Grid>
          <Grid id="footer-container" item xs={12} className={classes.footer}>
            <Footer/>
          </Grid>
          <Dialog open={selectContentLanguageOpen} onBackdropClick={this.handleCloseSelectLanguage}>
            <DialogTitle>
              <Typography>{t('choose_language')}</Typography>
            </DialogTitle>
            <DialogContent>
              {[{ key: 'lan_en', language: 'en' }, { key: 'lan_de', language: 'de' }, { key: 'lan_fa', language: 'fa' }]
                .map(lan => (
                  <Button variant={'text'} fullWidth
                          onClick={this.handleSelectContentLanguage(lan.language)}>{t(lan.key)}</Button>
                ))}

            </DialogContent>
          </Dialog>
        </Grid>
      );
    }
  }
}

export default withRouter(withNamespaces('translation')(withStyles(AppStyles)(App)));
