import axios from 'axios';

const API = `${process.env.REACT_APP_API_URL}/v1`;
export const getComponents = (categoryA, categoryB) =>
    axios.get(`${API}/component/${categoryA}/${categoryB}`);

export const createCompoent = ({title, categories, contentLanguage}) =>
    axios.post(`${API}/component`, {
        title,
        categories,
        languages: [contentLanguage],
    });


export const getRooms = (component, variable) =>
    axios.get(`${API}/room/${component}/${variable}/list`);

export const createRoom = (title, component, variable) =>
    axios.post(`${API}/room`, {
        title,
        component,
        variable,
    });

export const getComponentDetail = (id) => axios.get(`${API}/component/${id}`);

export const getCategoryAList = () => axios.get(`${API}/category/a-list`);
export const getCategoryBList = (categoryA) => axios.get(`${API}/category/a-list/${categoryA}/b-list`);
export const getAllCategoryBList = () => axios.get(`${API}/category/b-list`);

export const contributeToComponent = (component, title, language) => axios.post(`${API}/component/${component}/contribute`, {
    title,
    language,
});

export const likeCategoryA = (category) => axios.post(`${API}/category/a/${category}/like`);

export const likeCategoryB = (category) => axios.post(`${API}/category/b/${category}/like`);
export const likeComponent = (component) => axios.post(`${API}/component/${component}/like`);
export const likeVariable = (variable) => axios.post(`${API}/variable/${variable}/like`);
export const likeRoom = (room) => axios.post(`${API}/room/${room}/like`);
export const getVariables = () => axios.get(`${API}/variable`);

export const createPost = (room, title, body, image, category = '') => {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('body', body);
    formData.append('categories[0]', category);
    formData.append('coverImage', image);
    return axios.post(`${API}/room/${room}/post`, formData, {
        headers: {
            'content-type': 'multipart/form-data',
        },
    });
};

export const getPosts = (room) => axios.get(`${API}/room/${room}/post-list`);
export const getAllComponents = () => axios.get(`${API}/component`);
export const getRoomDetails = (roomId) => axios.get(`${API}/room/${roomId}`);
