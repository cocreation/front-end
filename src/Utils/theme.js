import { createMuiTheme } from '@material-ui/core';
import { grey, green, lightGreen } from '@material-ui/core/colors';

export default lan =>
  createMuiTheme({
    direction: lan === 'fa' ? 'rtl' : 'ltr',
    palette: {
      primary: green,
      secondary: grey,
      text: {
        primary: '#000000',
        disabled: grey['500'],
        hint: lightGreen['200'],
        secondary: lightGreen['500'],
      },
    },
  });
